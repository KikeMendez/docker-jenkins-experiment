start:
	docker run -d --name jenkins_server -u 1000:1000 -p 8080:8080 -p 50000:50000 -v /home/ubuntu/jenkins:/var/jenkins_home jenkins_server:latest

build:
	docker build -t jenkins_server .
